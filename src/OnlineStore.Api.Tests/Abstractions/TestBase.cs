﻿using Microsoft.Extensions.DependencyInjection;
using OnlineStore.Api.Client;
using RestEase.HttpClientFactory;

namespace OnlineStore.Api.Tests.Abstractions;

public abstract class TestBase
{
	protected const string BaseAddress = "http://localhost:10000";

	protected readonly IOnlineStoreClient Client;

	protected TestBase()
	{
		var services = new ServiceCollection();

		services.AddRestEaseClient<IOnlineStoreClient>(BaseAddress);


		var serviceProvider = services.BuildServiceProvider();

		Client = serviceProvider.GetRequiredService<IOnlineStoreClient>();
	}
}
