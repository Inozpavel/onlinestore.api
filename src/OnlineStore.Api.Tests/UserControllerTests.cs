﻿using System.Net;
using OnlineStore.Api.Client.Models.Users;
using OnlineStore.Api.Tests.Abstractions;
using RestEase;
using Xunit;
using Xunit.Abstractions;

namespace OnlineStore.Api.Tests;

public class UserControllerTests : TestBase
{
	private readonly ITestOutputHelper _testOutputHelper;

	public UserControllerTests(ITestOutputHelper testOutputHelper)
	{
		_testOutputHelper = testOutputHelper;
	}

	[Fact]
	public async Task CanRegisterAccount()
	{
		var model = GenerateRegisterModel();
		await Client.RegisterNewUser(model);

		Assert.True(true);
	}

	[Fact]
	public async Task CanRegisterAccountAndGetToken()
	{
		var model = GenerateRegisterModel();
		await Client.RegisterNewUser(model);

		var response = await Client.GetToken(new GetTokenRequestModel
		{
			Email = model.Email,
			Password = model.Password
		});

		_testOutputHelper.WriteLine($"Token is {response?.Token}");

		Assert.NotEmpty(response?.Token ?? "");
	}

	[Fact]
	public async Task CanGetProfile()
	{
		var model = GenerateRegisterModel();
		await Client.RegisterNewUser(model);

		var response = await Client.GetToken(new GetTokenRequestModel
		{
			Email = model.Email,
			Password = model.Password
		});

		Client.Token = response.TokenType + " " + response.Token;

		var result = await Client.GetProfile();

		Assert.NotEmpty(result?.Email ?? "");
	}

	[Fact]
	public async Task CanGetRoles()
	{
		var model = GenerateRegisterModel();
		await Client.RegisterNewUser(model);

		var response = await Client.GetToken(new GetTokenRequestModel
		{
			Email = model.Email,
			Password = model.Password
		});

		Client.Token = response.TokenType + " " + response.Token;

		var result = await Client.GetRoles();

		Assert.NotEmpty(result);
	}

	[Fact]
	public async Task GetRolesWithoutTokenReturn401()
	{
		Exception exception = null;

		try
		{
			await Client.GetRoles();
		}
		catch (Exception e)
		{
			exception = e;
		}

		Assert.True(exception is ApiException { StatusCode: HttpStatusCode.Unauthorized });
	}


	[Fact]
	public async Task ProfileWithoutTokenReturns401()
	{
		Exception exception = null;

		try
		{
			await Client.GetProfile();
		}
		catch (Exception e)
		{
			exception = e;
		}

		Assert.True(exception is ApiException { StatusCode: HttpStatusCode.Unauthorized });
	}

	[Fact]
	public async Task IncorrectRegistrationDataReturns400()
	{
		Exception exception = null;

		try
		{
			await Client.RegisterNewUser(new RegisterUserRequestModel());
		}
		catch (Exception e)
		{
			exception = e;
		}

		Assert.True(exception is ApiException { StatusCode: HttpStatusCode.BadRequest });
	}

	private static RegisterUserRequestModel GenerateRegisterModel()
	{
		var password = Guid.NewGuid().ToString();

		var model = new RegisterUserRequestModel
		{
			Email = Guid.NewGuid() + "@mail.ru",
			Password = password,
			PasswordConfirmation = password,
			Username = "TestUser_" + Guid.NewGuid(),
		};

		return model;
	}
}
