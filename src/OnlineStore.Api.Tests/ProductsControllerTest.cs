using OnlineStore.Api.Client.Models.Products;
using OnlineStore.Api.Tests.Abstractions;
using Xunit;

namespace OnlineStore.Api.Tests;

public class ProductsControllerTest : TestBase
{

	[Fact]
	public async Task CanGetProducts()
	{
		var companies = await Client.GetProducts(new GetProductListRequestModel());

		Assert.NotNull(companies?.Items);
	}
}
