﻿using System.Security.Cryptography;
using System.Text;
using OnlineStore.Api.Services.Abstractions;

namespace OnlineStore.Api.Services;

public class CryptoService : ICryptoService
{
	public string HashWithSha256(string input)
	{
		var sha256 = SHA256.Create();
		var hashBytes = sha256.ComputeHash(Encoding.UTF8.GetBytes(input));
		var hash = Convert.ToHexString(hashBytes);

		return hash;
	}
}
