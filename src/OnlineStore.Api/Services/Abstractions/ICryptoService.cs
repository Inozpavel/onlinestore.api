﻿namespace OnlineStore.Api.Services.Abstractions;

public interface ICryptoService
{
	string HashWithSha256(string input);
}
