﻿namespace OnlineStore.Api.Application.Enums;

public enum UserRole
{
	Default,
	Moderator,
	Admin,
	SuperAdmin,
}
