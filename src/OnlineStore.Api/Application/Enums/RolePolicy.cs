﻿namespace OnlineStore.Api.Application.Enums;

public enum RolePolicy
{
	MinimumModeratorRole,
	MinimumAdminRole,
	MinimumSuperAdminRole,
}
