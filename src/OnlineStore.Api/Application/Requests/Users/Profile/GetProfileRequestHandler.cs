﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using OnlineStore.Api.Infrastructure.Abstractions;
using OnlineStore.Api.Client.Models.Users;

namespace OnlineStore.Api.Application.Requests.Users.Profile;

public class GetProfileRequestHandler : IRequestHandler<GetProfileRequest, ProfileResponseModel>
{
	private readonly IReadOnlyRepository _readOnlyRepository;

	public GetProfileRequestHandler(IReadOnlyRepository readOnlyRepository)
	{
		_readOnlyRepository = readOnlyRepository;
	}

	public async Task<ProfileResponseModel> Handle(GetProfileRequest request, CancellationToken cancellationToken)
	{
		var user = await _readOnlyRepository.Users
			.Include(x => x.Role)
			.FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken);

		if (user == null)
		{
			throw new ApplicationException($"User with id {request.Id} is not found!");
		}

		var responseModel = new ProfileResponseModel
		{
			Email = user.Email,
			Username = user.Username,
			Created = user.Created,
			RoleName = user.Role.Id,
			Phone = user.PhoneNumber,
		};

		return responseModel;
	}
}
