﻿using System;
using MediatR;
using OnlineStore.Api.Client.Models.Users;

namespace OnlineStore.Api.Application.Requests.Users.Profile;

public record GetProfileRequest(Guid Id) : IRequest<ProfileResponseModel>;
