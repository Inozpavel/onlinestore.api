﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using OnlineStore.Api.Infrastructure.Abstractions;
using OnlineStore.Api.Options;
using OnlineStore.Api.Services.Abstractions;
using OnlineStore.Api.Client.Models.Users;

namespace OnlineStore.Api.Application.Requests.Users.Token;

public class GetTokenRequestHandler : IRequestHandler<GetTokenRequest, TokenResponseModel>
{
	private readonly IReadOnlyRepository _readOnlyRepository;
	private readonly ICryptoService _cryptoService;
	private readonly JwtOptions _jwtOptions;

	public GetTokenRequestHandler(IReadOnlyRepository readOnlyRepository, ICryptoService cryptoService,
		IOptions<JwtOptions> jwtOptions)
	{
		_readOnlyRepository = readOnlyRepository;
		_cryptoService = cryptoService;
		_jwtOptions = jwtOptions.Value;
	}

	public async Task<TokenResponseModel> Handle(GetTokenRequest request, CancellationToken cancellationToken)
	{
		var hash = _cryptoService.HashWithSha256(request.Password);

		var user = await _readOnlyRepository.Users
			.Include(x => x.Role)
			.FirstOrDefaultAsync(x => x.Email == request.Email && x.PasswordHash == hash, cancellationToken);

		if (user == null)
		{
			throw new ArgumentException(nameof(user));
		}

		var claims = new List<Claim>
		{
			new (ClaimTypes.NameIdentifier, user.Id.ToString()),
			new (ClaimTypes.Role, user.Role.Id),
			new (ClaimTypes.Email, user.Email),
			new (ClaimTypes.Name, user.Username),
		};

		var token = new JwtSecurityToken(
			_jwtOptions.Issuer,
			null,
			claims,
			DateTime.Now,
			DateTime.Now.AddMinutes(_jwtOptions.ExpireIntervalInMinutes),
			new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtOptions.SecretKey)),
				SecurityAlgorithms.HmacSha256));

		var encodedJwt = new JwtSecurityTokenHandler().WriteToken(token);

		var response = new TokenResponseModel
		{
			TokenType = JwtBearerDefaults.AuthenticationScheme,
			Token = encodedJwt,
			Expiration = _jwtOptions.ExpireIntervalInMinutes,
		};

		return response;
	}
}
