﻿using MediatR;
using OnlineStore.Api.Client.Models.Users;

namespace OnlineStore.Api.Application.Requests.Users.Token;

public class GetTokenRequest : GetTokenRequestModel, IRequest<TokenResponseModel>
{
}
