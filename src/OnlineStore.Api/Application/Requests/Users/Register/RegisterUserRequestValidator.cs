﻿using System.Text.RegularExpressions;
using FluentValidation;

namespace OnlineStore.Api.Application.Requests.Users.Register;

public class RegisterUserRequestValidator : AbstractValidator<RegisterUserRequest>
{
	private const int MinimumPasswordLength = 5;
	
	public RegisterUserRequestValidator()
	{
		RuleFor(x => x.Email)
			.EmailAddress()
			.NotEmpty();
		
		RuleFor(x => x.Password)
			.MinimumLength(MinimumPasswordLength);
		
		RuleFor(x => x.Password)
			.Must(s => Regex.IsMatch(s, "[a-z]", RegexOptions.IgnoreCase))
			.WithMessage("'Password' must contain a letter!");
		
		RuleFor(x => x.Password)
			.Must(s => Regex.IsMatch(s, "[0-9]", RegexOptions.IgnoreCase))
			.WithMessage("'Password' must contain a digit!");
		
		RuleFor(x => x.PasswordConfirmation)
			.Equal(x => x.Password)
			.WithMessage("'Password Confirmation' must be equal to 'Password'");
		
		RuleFor(x => x.Username)
			.NotEmpty();
	}
}
