﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using OnlineStore.Api.Application.Enums;
using OnlineStore.Api.Entities;
using OnlineStore.Api.Infrastructure.Abstractions;
using OnlineStore.Api.Services.Abstractions;

namespace OnlineStore.Api.Application.Requests.Users.Register;

public class RegisterUserRequestHandler : IRequestHandler<RegisterUserRequest>
{
	private readonly IRepository _repository;
	private readonly ICryptoService _cryptoService;

	public RegisterUserRequestHandler(IRepository repository, ICryptoService cryptoService)
	{
		_repository = repository;
		_cryptoService = cryptoService;
	}

	public async Task<Unit> Handle(RegisterUserRequest request, CancellationToken cancellationToken)
	{
		if (await _repository.Users.AnyAsync(x => x.Email == request.Email, cancellationToken))
		{
			throw new ArgumentException($"User with email {request.Email} is already registered!");
		}

		var hash = _cryptoService.HashWithSha256(request.Password);
		var role = await _repository.FindOrCreateRole(nameof(UserRole.Default));

		var user = new User(request.Email, hash, request.Username, request.PhoneNumber, role);

		await _repository.AddAsync(user, cancellationToken);
		await _repository.SaveChangesAsync(cancellationToken);

		return Unit.Value;
	}
}
