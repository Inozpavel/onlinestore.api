﻿using MediatR;
using OnlineStore.Api.Client.Models.Users;

namespace OnlineStore.Api.Application.Requests.Users.Register;

public class RegisterUserRequest : RegisterUserRequestModel, IRequest
{
}
