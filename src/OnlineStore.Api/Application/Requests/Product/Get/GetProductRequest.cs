﻿using System;
using MediatR;
using OnlineStore.Api.Client.Models.Products;

namespace OnlineStore.Api.Application.Requests.Product.Get;

public class GetProductRequest : IRequest<ProductResponseModel>
{
	public GetProductRequest(Guid id)
	{
		Id = id;
	}

	public Guid Id { get; }
}
