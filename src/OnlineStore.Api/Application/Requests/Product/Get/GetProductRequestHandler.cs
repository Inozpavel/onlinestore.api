﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using OnlineStore.Api.Client.Models.Products;
using OnlineStore.Api.Infrastructure.Abstractions;

namespace OnlineStore.Api.Application.Requests.Product.Get;

public class GetProductRequestHandler : IRequestHandler<GetProductRequest, ProductResponseModel>
{
	private readonly IReadOnlyRepository _readOnlyRepository;
	private readonly IMapper _mapper;

	public GetProductRequestHandler(IReadOnlyRepository readOnlyRepository, IMapper mapper)
	{
		_readOnlyRepository = readOnlyRepository;
		_mapper = mapper;
	}

	public async Task<ProductResponseModel> Handle(GetProductRequest request, CancellationToken cancellationToken)
	{
		var product = await _readOnlyRepository.Products.FirstOrDefaultAsync(
			x => x.Id == request.Id,
			cancellationToken);

		return _mapper.Map<ProductResponseModel>(product);
	}
}
