﻿using MediatR;
using OnlineStore.Api.Client.Models;
using OnlineStore.Api.Client.Models.Products;

namespace OnlineStore.Api.Application.Requests.Product.Get;

public class GetProductListRequest : GetProductListRequestModel, IRequest<CollectionModel<ProductResponseModel>>
{
}
