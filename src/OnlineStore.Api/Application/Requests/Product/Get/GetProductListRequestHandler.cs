﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using OnlineStore.Api.Client.Models;
using OnlineStore.Api.Client.Models.Products;
using OnlineStore.Api.Infrastructure.Abstractions;

namespace OnlineStore.Api.Application.Requests.Product.Get;

public class GetProductListRequestHandler :
	IRequestHandler<GetProductListRequest, CollectionModel<ProductResponseModel>>
{
	private readonly IReadOnlyRepository _readOnlyRepository;
	private readonly IMapper _mapper;

	public GetProductListRequestHandler(IReadOnlyRepository readOnlyRepository, IMapper mapper)
	{
		_readOnlyRepository = readOnlyRepository;
		_mapper = mapper;
	}

	public async Task<CollectionModel<ProductResponseModel>> Handle(
		GetProductListRequest request,
		CancellationToken cancellationToken)
	{
		var queryable = _readOnlyRepository.Products.AsQueryable();

		if (!string.IsNullOrWhiteSpace(request.Query))
		{
			var pattern = $"%{request.Query}%";
			queryable = queryable.Where(x => EF.Functions.ILike(x.Name, pattern));
		}

		var entities = await queryable
			.OrderByDescending(x => x.Created)
			.Skip(request.Skip)
			.Take(request.Take)
			.ToArrayAsync(cancellationToken);

		var items = _mapper.Map<ProductResponseModel[]>(entities);

		return new CollectionModel<ProductResponseModel>(items);
	}
}
