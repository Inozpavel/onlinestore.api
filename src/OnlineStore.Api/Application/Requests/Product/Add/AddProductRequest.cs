﻿using MediatR;
using OnlineStore.Api.Client.Models.Products;

namespace OnlineStore.Api.Application.Requests.Product.Add;

public class AddProductRequest : AddProductRequestModel, IRequest
{
}
