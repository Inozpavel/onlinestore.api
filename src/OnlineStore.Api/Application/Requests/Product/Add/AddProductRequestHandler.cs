﻿using MediatR;
using OnlineStore.Api.Infrastructure.Abstractions;

namespace OnlineStore.Api.Application.Requests.Product.Add;

public class AddProductRequestHandler : IRequestHandler<AddProductRequest>
{
	private readonly IRepository _repository;

	public AddProductRequestHandler(IRepository repository)
	{
		_repository = repository;
	}

	public async Task<Unit> Handle(AddProductRequest request, CancellationToken cancellationToken)
	{
		var product = new Entities.Product(request.Name, request.Description, request.Cost);

		await _repository.AddAsync(product, cancellationToken);

		await _repository.SaveChangesAsync(cancellationToken);
		return Unit.Value;
	}
}
