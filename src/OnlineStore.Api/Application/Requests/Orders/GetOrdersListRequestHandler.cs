﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using OnlineStore.Api.Client.Models;
using OnlineStore.Api.Client.Models.Orders;
using OnlineStore.Api.Infrastructure.Abstractions;

namespace OnlineStore.Api.Application.Requests.Orders;

public class GetOrdersListRequestHandler : IRequestHandler<GetOrdersListRequest, CollectionModel<OrderResponseModel>>
{
	private readonly IReadOnlyRepository _readOnlyRepository;
	private readonly IMapper _mapper;

	public GetOrdersListRequestHandler(IReadOnlyRepository readOnlyRepository, IMapper mapper)
	{
		_readOnlyRepository = readOnlyRepository;
		_mapper = mapper;
	}

	public async Task<CollectionModel<OrderResponseModel>> Handle(
		GetOrdersListRequest request,
		CancellationToken cancellationToken)
	{
		var entities = await _readOnlyRepository.Orders
			.Include(x => x.ProductsInOrder)
			.ThenInclude(x => x.Product)
			.OrderByDescending(x => x.Created)
			.Skip(request.Skip)
			.Take(request.Take)
			.ToArrayAsync(cancellationToken);

		var items = _mapper.Map<OrderResponseModel[]>(entities);

		return new CollectionModel<OrderResponseModel>(items);
	}
}
