﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using OnlineStore.Api.Infrastructure.Abstractions;

namespace OnlineStore.Api.Application.Requests.Orders;

public class CloseActiveOrderRequestHandler : IRequestHandler<CloseActiveOrderRequest>
{
	private readonly IRepository _repository;

	public CloseActiveOrderRequestHandler(IRepository repository)
	{
		_repository = repository;
	}

	public async Task<Unit> Handle(CloseActiveOrderRequest request, CancellationToken cancellationToken)
	{
		var user = await _repository.Users
			.Include(x => x.ActiveOrder)
			.ThenInclude(x => x.ProductsInOrder)
			.ThenInclude(x => x.Product)
			.Include(x => x.ClosedOrders)
			.FirstOrDefaultAsync(x => x.Id == request.UserId, cancellationToken);

		if (user == null)
		{
			throw new InvalidOperationException($"User with id {request.UserId} is not found");
		}

		if (user.ActiveOrder == null)
		{
			throw new InvalidOperationException("User has not active order");
		}

		user.CloseActiveOrder();

		await _repository.SaveChangesAsync(cancellationToken);
		return Unit.Value;
	}
}
