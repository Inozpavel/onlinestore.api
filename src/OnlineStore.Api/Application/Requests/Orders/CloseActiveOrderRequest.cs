﻿using MediatR;

namespace OnlineStore.Api.Application.Requests.Orders;

public class CloseActiveOrderRequest : IRequest
{
	public Guid UserId { get; set; }
}
