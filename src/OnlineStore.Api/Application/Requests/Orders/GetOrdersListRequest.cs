﻿using MediatR;
using OnlineStore.Api.Client.Models;
using OnlineStore.Api.Client.Models.Orders;

namespace OnlineStore.Api.Application.Requests.Orders;

public class GetOrdersListRequest : GetOrdersListRequestModel, IRequest<CollectionModel<OrderResponseModel>>
{
}
