﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using OnlineStore.Api.Entities;
using OnlineStore.Api.Infrastructure.Abstractions;

namespace OnlineStore.Api.Application.Requests.Cart;

public class AddProductToCartRequestHandler : IRequestHandler<AddProductToCartRequest>
{
	private readonly IRepository _repository;

	public AddProductToCartRequestHandler(IRepository repository)
	{
		_repository = repository;
	}

	public async Task<Unit> Handle(AddProductToCartRequest request, CancellationToken cancellationToken)
	{
		var user = await _repository.Users
			.Include(x => x.ActiveOrder)
			.FirstOrDefaultAsync(x => x.Id == request.UserId, cancellationToken);

		if (user == null)
		{
			throw new ArgumentException($"{nameof(request.UserId)} is incorrect");
		}

		var product = await _repository.Products.FirstOrDefaultAsync(x => x.Id == request.ProductId, cancellationToken);

		var productInOrder = new ProductInOrder(product, request.Count);

		if (user.ActiveOrder == null)
		{
			user.CreateActiveOrder(productInOrder);
		}

		await _repository.SaveChangesAsync(cancellationToken);
		return Unit.Value;
	}
}
