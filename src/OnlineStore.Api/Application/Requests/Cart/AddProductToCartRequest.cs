﻿using MediatR;
using OnlineStore.Api.Client.Models.Products;

namespace OnlineStore.Api.Application.Requests.Cart;

public class AddProductToCartRequest : AddProductToCartRequestModel, IRequest
{
	public Guid ProductId { get; set; }

	public Guid UserId { get; set; }
}
