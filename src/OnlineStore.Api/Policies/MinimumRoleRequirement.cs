﻿using Microsoft.AspNetCore.Authorization;
using OnlineStore.Api.Application.Enums;

namespace OnlineStore.Api.Policies;

public class MinimumRoleRequirement : IAuthorizationRequirement
{
	public MinimumRoleRequirement(UserRole userRole)
	{
		UserRole = userRole;
	}

	public UserRole UserRole { get; }
}
