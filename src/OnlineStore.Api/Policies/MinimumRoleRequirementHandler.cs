﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using OnlineStore.Api.Application.Enums;

namespace OnlineStore.Api.Policies;

public class MinimumRoleRequirementHandler : AuthorizationHandler<MinimumRoleRequirement>
{
	protected override Task HandleRequirementAsync(AuthorizationHandlerContext context,
		MinimumRoleRequirement requirement)
	{

		var roleName = context.User.FindFirst(c => c.Type == ClaimTypes.Role)?.Value;

		if (roleName == null)
		{
			return Task.CompletedTask;
		}

		if (!Enum.TryParse(typeof(UserRole), roleName, true, out var roleValue))
		{
			return Task.CompletedTask;
		}

		if (roleValue != null && (UserRole)roleValue >= requirement.UserRole)
		{
			context.Succeed(requirement);
		}

		return Task.CompletedTask;
	}
}
