using Inozpavel.Microservices.Platform;
using Inozpavel.Microservices.Platform.Common.Enums;

namespace OnlineStore.Api;

public static class Program
{
	public static void Main(string[] args) => CreateHostBuilder(args).Build().Run();

	private static IHostBuilder CreateHostBuilder(string[] args) =>
		Host
			.CreateDefaultBuilder(args)
			.UsePlatform<Startup>(options => options.SwaggerSecurity = SwaggerSecurity.JwtBearer);
}
