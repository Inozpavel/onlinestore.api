﻿using System;
using System.Collections.Generic;
using System.Linq;

// ReSharper disable UnusedAutoPropertyAccessor.Local

namespace OnlineStore.Api.Entities;

public class Order : BaseEntity
{
	public Order(List<ProductInOrder> productsInOrder, Guid userId, User user, DateTimeOffset orderTime) : this()
	{
		ProductsInOrder = productsInOrder;
		UserId = userId;
		User = user;
		OrderTime = orderTime;
	}

	public Order(ProductInOrder product) : this()
	{
		ProductsInOrder = new List<ProductInOrder> { product };
	}

	private Order()
	{
		Id = Guid.NewGuid();
	}

	public List<ProductInOrder> ProductsInOrder { get; private set; }

	public Guid UserId { get; private set; }

	public User User { get; private set; }

	public User ActiveOnUser { get; private set; }

	public DateTimeOffset OrderTime { get; private set; }

	public void Close()
	{
		ProductsInOrder = ProductsInOrder.Select(x =>
		{
			x.SetActualCost();
			return x;
		}).ToList();
	}
}
