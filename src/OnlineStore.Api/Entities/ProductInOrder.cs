﻿using System;

namespace OnlineStore.Api.Entities;

public class ProductInOrder
{
	public ProductInOrder(Product product, int count)
	{
		Product = product;
		Count = count;
	}

	private ProductInOrder()
	{
	}

	public Guid ProductId { get; private set; }

	public Product Product { get; private set; }

	public Guid OrderId { get; private set; }

	public Order Order { get; private set; }

	public int Count { get; private set; }

	public decimal Cost { get; private set; }

	public void SetActualCost() => Cost = Product.Cost;
}
