﻿using System.Collections.Generic;

namespace OnlineStore.Api.Entities;

public class Role
{
	public Role(string id, string name = null, string? description = null) : this()
	{
		Id = id;
		Name = name;
		Description = description;
	}

	private Role()
	{
	}

	public string Id { get; private set; }

	public string Name { get; private set; }

	public string? Description { get; private set; }

	public List<User> Users { get; private set; }
}
