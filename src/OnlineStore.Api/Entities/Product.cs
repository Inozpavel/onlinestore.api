﻿using System;

// ReSharper disable UnusedAutoPropertyAccessor.Local
// ReSharper disable CollectionNeverUpdated.Global

namespace OnlineStore.Api.Entities;

public class Product : BaseEntity
{
	public Product(string name, string description, decimal cost)
	{
		Name = name;
		Description = description;
		Cost = cost;
	}

	private Product()
	{
		Id = Guid.NewGuid();
	}

	public Guid Id { get; private set; }

	public string Name { get; private set; }

	public string Description { get; private set; }

	public decimal Cost { get; private set; }

	public byte[] Image { get; private set; }
}
