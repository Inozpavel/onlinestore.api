﻿using System;

namespace OnlineStore.Api.Entities;

public abstract class BaseEntity
{
	protected BaseEntity()
	{
		Created = DateTimeOffset.UtcNow;
	}

	public Guid Id { get; set; }
	
	public DateTimeOffset Created { get; private set; }

	public DateTimeOffset? Updated { get; protected set; }
}
