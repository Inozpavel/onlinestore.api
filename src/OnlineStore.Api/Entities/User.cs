﻿namespace OnlineStore.Api.Entities;

public class User : BaseEntity
{
	public User(string email, string passwordHash, string username, string phoneNumber, Role role) : this()
	{
		Email = email;
		PasswordHash = passwordHash;
		Username = username;
		PhoneNumber = phoneNumber;
		Role = role;
	}

	private User()
	{
		Id = Guid.NewGuid();
	}

	public Guid Id { get; private set; }

	public string Email { get; private set; }

	public string PhoneNumber { get; private set; }

	public string PasswordHash { get; private set; }

	public string Username { get; private set; }

	public string RoleId { get; private set; }

	public Role Role { get; private set; }

	public Guid? ActiveOrderId { get; private set; }

	public Order ActiveOrder { get; private set; }

	public List<Order> ClosedOrders { get; private set; }

	public void CreateActiveOrder(ProductInOrder productInOrder)
	{
		ActiveOrder = new Order(productInOrder);
	}

	public void CloseActiveOrder()
	{
		ActiveOrder.Close();
		ClosedOrders.Add(ActiveOrder);
		ActiveOrder = null;
	}
}
