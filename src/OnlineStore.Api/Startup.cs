using FluentValidation.AspNetCore;
using Hellang.Middleware.ProblemDetails;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using OnlineStore.Api.Application.Enums;
using OnlineStore.Api.Infrastructure;
using OnlineStore.Api.Infrastructure.Abstractions;
using OnlineStore.Api.Options;
using OnlineStore.Api.Policies;
using OnlineStore.Api.Services;
using OnlineStore.Api.Services.Abstractions;
using Prometheus.Client.HttpRequestDurations;
using OnlineStore.Api.Extension;

namespace OnlineStore.Api;

public class Startup
{
	private readonly IConfiguration _configuration;

	public Startup(IConfiguration configuration)
	{
		_configuration = configuration;
	}

	public void ConfigureServices(IServiceCollection services)
	{
		var connectionString = _configuration.GetConnectionString(nameof(DataContext));

		services
			.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
			.AddJwtBearerAuthentication(_configuration);

		services.AddAuthorization(options =>
		{
			options.AddPolicy(nameof(RolePolicy.MinimumModeratorRole), policy =>
				policy.Requirements.Add(new MinimumRoleRequirement(UserRole.Moderator)));

			options.AddPolicy(nameof(RolePolicy.MinimumAdminRole), policy =>
				policy.Requirements.Add(new MinimumRoleRequirement(UserRole.Admin)));

			options.AddPolicy(nameof(RolePolicy.MinimumSuperAdminRole), policy =>
				policy.Requirements.Add(new MinimumRoleRequirement(UserRole.SuperAdmin)));
		});

		services
			.AddProblemDetailsExtension()
			.AddMediatR(typeof(Startup))
			.AddAutoMapper(typeof(Startup));
		
		services
			.AddDbContext<DataContext>(options => options.UseNpgsql(connectionString))
			.AddDbContext<ReadOnlyDataContext>(options => options.UseNpgsql(connectionString))
			.AddScoped<IRepository, DataContext>()
			.AddScoped<IReadOnlyRepository, ReadOnlyDataContext>()
			.Configure<JwtOptions>(_configuration.GetSection(nameof(JwtOptions)).Bind)
			.AddTransient<ICryptoService, CryptoService>()
			.AddSingleton<IAuthorizationHandler, MinimumRoleRequirementHandler>()
			;

		services.AddControllers().AddFluentValidation(fv =>
		{
			fv.RegisterValidatorsFromAssemblyContaining<Startup>();
			fv.LocalizationEnabled = false;
		});
	}

	public void Configure(IApplicationBuilder app)
	{
		app.EnsureMigrationOfContext<DataContext>();
		app.UseProblemDetails();
		app.UsePrometheusRequestDurations(options =>
		{
			options.IncludeMethod = true;
			options.IncludePath = true;
			options.IncludeStatusCode = true;
		});

		app.UseRouting();

		app.UseAuthentication();
		app.UseAuthorization();

		app.UseEndpoints(endpoints => endpoints.MapControllers());
	}
}
