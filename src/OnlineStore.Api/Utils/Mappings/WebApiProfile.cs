﻿using AutoMapper;
using OnlineStore.Api.Entities;
using OnlineStore.Api.Client.Models.Orders;
using OnlineStore.Api.Client.Models.Products;

namespace OnlineStore.Api.Utils.Mappings;

public class WebApiProfile : Profile
{
	public WebApiProfile()
	{
		CreateMap<Product, ProductResponseModel>();

		CreateMap<ProductInOrder, OrderProductResponseModel>()
			.ForMember(dest => dest.ProductName, opt => opt.MapFrom(src => src.Product.Name));
		CreateMap<Order, OrderResponseModel>()
			.ForMember(dest => dest.Products, opt => opt.MapFrom(src => src.ProductsInOrder));
		
	}
}
