﻿using Microsoft.EntityFrameworkCore;

namespace OnlineStore.Api.Infrastructure;

public class ReadOnlyDataContext : DataContext
{
	public ReadOnlyDataContext(DbContextOptions<DataContext> options) : base(options)
	{
		ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
	}
}
