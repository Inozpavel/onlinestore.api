﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OnlineStore.Api.Entities;

namespace OnlineStore.Api.Infrastructure.Configurations;

public class UserConfiguration : BaseEntityConfiguration<User>
{
	protected override string TableName => "Users";

	public override void Configure(EntityTypeBuilder<User> builder)
	{
		builder
			.Property(x => x.Email)
			.IsRequired();

		builder
			.Property(x => x.PasswordHash)
			.IsRequired();

		builder
			.HasOne(x => x.Role)
			.WithMany(x => x.Users)
			.HasForeignKey(x => x.RoleId)
			.IsRequired();

		builder.HasMany(x => x.ClosedOrders)
			.WithOne(x => x.User)
			.HasForeignKey(x => x.UserId);

		builder
			.HasOne(x => x.ActiveOrder)
			.WithOne(x => x.ActiveOnUser)
			.HasForeignKey<User>(x => x.ActiveOrderId);

		builder
			.HasIndex(x => x.Email)
			.IsUnique();

		base.Configure(builder);
	}
}
