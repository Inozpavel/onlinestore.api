﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OnlineStore.Api.Entities;

namespace OnlineStore.Api.Infrastructure.Configurations;

public class ProductConfiguration : BaseEntityConfiguration<Product>
{
	protected override string TableName => "Products";

	public override void Configure(EntityTypeBuilder<Product> builder)
	{
		builder.Property(x => x.Name).IsRequired();

		base.Configure(builder);
	}
}
