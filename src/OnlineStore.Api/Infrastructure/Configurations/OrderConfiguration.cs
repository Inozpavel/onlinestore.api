﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OnlineStore.Api.Entities;

namespace OnlineStore.Api.Infrastructure.Configurations;

public class OrderConfiguration : BaseEntityConfiguration<Order>
{
	protected override string TableName => "Orders";

	public override void Configure(EntityTypeBuilder<Order> builder)
	{
		builder
			.HasMany(x => x.ProductsInOrder)
			.WithOne(x => x.Order)
			.HasForeignKey(x => x.OrderId);

		builder.HasIndex(x => new
		{
			x.UserId,
			x.OrderTime,
		});

		base.Configure(builder);
	}
}
