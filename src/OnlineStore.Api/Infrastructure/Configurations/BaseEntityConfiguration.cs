﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OnlineStore.Api.Entities;

namespace OnlineStore.Api.Infrastructure.Configurations;

public abstract class BaseEntityConfiguration<TEntity> : IEntityTypeConfiguration<TEntity> where TEntity : BaseEntity
{
	protected abstract string TableName { get; }

	public virtual void Configure(EntityTypeBuilder<TEntity> builder)
	{
		builder.ToTable(TableName);
		
		builder.HasKey(x => x.Id);

		builder
			.Property(x => x.Created)
			.HasDefaultValueSql("current_timestamp");
	}
}
