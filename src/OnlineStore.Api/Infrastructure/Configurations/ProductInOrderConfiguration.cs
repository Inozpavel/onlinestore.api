﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OnlineStore.Api.Entities;

namespace OnlineStore.Api.Infrastructure.Configurations;

public class ProductInOrderConfiguration : IEntityTypeConfiguration<ProductInOrder>
{
	public void Configure(EntityTypeBuilder<ProductInOrder> builder)
	{
		builder.ToTable("Products in order");

		builder.HasKey(x => new
		{
			x.OrderId,
			x.ProductId,
		});
	}
}
