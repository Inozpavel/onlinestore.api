﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OnlineStore.Api.Application.Enums;
using OnlineStore.Api.Entities;

namespace OnlineStore.Api.Infrastructure.Configurations;

public class RoleConfiguration : IEntityTypeConfiguration<Role>
{
	public void Configure(EntityTypeBuilder<Role> builder)
	{
		builder.ToTable("Roles");

		builder.HasKey(x => x.Id);

		builder
			.Property(x => x.Id)
			.IsRequired();

		builder
			.HasIndex(x => x.Id)
			.IsUnique();

		builder.HasData(Enum.GetNames(typeof(UserRole)).Select(name => new Role(name)));
	}
}
