﻿using OnlineStore.Api.Entities;

namespace OnlineStore.Api.Infrastructure.Abstractions;

public interface IRepository : IReadOnlyRepository
{
	Task AddAsync<TEntity>(TEntity entity, CancellationToken token) where TEntity : class;

	Task<Role> FindOrCreateRole(string name);

	Task<int> SaveChangesAsync(CancellationToken token);
}
