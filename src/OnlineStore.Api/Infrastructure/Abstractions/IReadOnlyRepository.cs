﻿using OnlineStore.Api.Entities;

// ReSharper disable UnusedMember.Global

namespace OnlineStore.Api.Infrastructure.Abstractions;

public interface IReadOnlyRepository
{
	IQueryable<Product> Products { get; }

	IQueryable<Order> Orders { get; }

	IQueryable<ProductInOrder> ProductInOrders { get; }

	IQueryable<User> Users { get; }

	IQueryable<Role> Roles { get; }
}
