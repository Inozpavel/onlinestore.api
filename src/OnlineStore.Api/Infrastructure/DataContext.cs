﻿using Microsoft.EntityFrameworkCore;
using OnlineStore.Api.Entities;
using OnlineStore.Api.Infrastructure.Abstractions;

namespace OnlineStore.Api.Infrastructure;

public class DataContext : DbContext, IRepository
{
	public DataContext(DbContextOptions<DataContext> options) : base(options)
	{
		ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.TrackAll;
	}

	#region IQueryable

	public IQueryable<Product> Products => DbCompanies;
	public IQueryable<Order> Orders => DbPromoCodes;
	public IQueryable<User> Users => DbUsers;
	public IQueryable<Role> Roles => DbRoles;
	public IQueryable<ProductInOrder> ProductInOrders => DbProductInOrders;

	#endregion

	#region DbSet

	public DbSet<Product> DbCompanies { get; set; }
	public DbSet<ProductInOrder> DbProductInOrders { get; set; }
	public DbSet<Order> DbPromoCodes { get; set; }
	public DbSet<User> DbUsers { get; set; }
	public DbSet<Role> DbRoles { get; set; }

	#endregion


	public new async Task AddAsync<TEntity>(TEntity entity, CancellationToken token) where TEntity : class
	{
		if (entity == null)
		{
			throw new ArgumentNullException(nameof(entity));
		}

		await base.AddAsync(entity, token);
	}

	public async Task<Role> FindOrCreateRole(string name) =>
		await DbRoles.FirstOrDefaultAsync(x => x.Id == name) ?? new Role(name);

	protected override void OnModelCreating(ModelBuilder modelBuilder)
	{
		modelBuilder.ApplyConfigurationsFromAssembly(typeof(DataContext).Assembly);

		base.OnModelCreating(modelBuilder);
	}
}
