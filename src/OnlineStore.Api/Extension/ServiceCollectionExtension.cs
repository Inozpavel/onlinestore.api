﻿using Hellang.Middleware.ProblemDetails;
using Microsoft.AspNetCore.Mvc;

namespace OnlineStore.Api.Extension;

public static class ServiceCollectionExtension
{
	public static IServiceCollection AddProblemDetailsExtension(this IServiceCollection services)
	{
		services.AddProblemDetails(options =>
		{
			options.Map<ArgumentException>(e => new ProblemDetails
			{
				Status = StatusCodes.Status400BadRequest,
				Title = e.Message,
			});
			options.Map<Exception>(e => new ProblemDetails
			{
				Title = e.Message,
				Type = e.GetType().Name,
				Status = StatusCodes.Status500InternalServerError,
				Detail = e.StackTrace
			});
		});

		return services;
	}
}
