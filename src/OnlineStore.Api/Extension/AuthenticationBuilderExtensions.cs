﻿using System.Text;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using OnlineStore.Api.Options;

namespace OnlineStore.Api.Extension;

public static class AuthenticationBuilderExtensions
{
	public static AuthenticationBuilder AddJwtBearerAuthentication(
		this AuthenticationBuilder builder,
		IConfiguration configuration)
	{
		var jwtOptions = configuration.GetSection(nameof(JwtOptions)).Get<JwtOptions>();

		builder.AddJwtBearer(options =>
		{
			options.IncludeErrorDetails = true;
			options.RequireHttpsMetadata = false;
			options.SaveToken = true;
			options.TokenValidationParameters = new TokenValidationParameters
			{
				ValidateIssuer = true,
				ValidIssuer = jwtOptions.Issuer,

				ValidateIssuerSigningKey = true,
				IssuerSigningKey =
					new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtOptions.SecretKey)),

				ValidateAudience = false,
				ValidateLifetime = false,
			};
		});

		return builder;
	}
}
