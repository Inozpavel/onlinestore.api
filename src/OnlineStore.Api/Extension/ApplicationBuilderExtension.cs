﻿using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace OnlineStore.Api.Extension;

public static class ApplicationBuilderExtension
{
	public static IApplicationBuilder EnsureMigrationOfContext<T>(this IApplicationBuilder builder)
		where T : DbContext
	{
		using var scope = builder.ApplicationServices.CreateScope();

		var logger = scope.ServiceProvider.GetRequiredService<ILogger<Startup>>();
		var context = scope.ServiceProvider.GetRequiredService<T>();
		var migrations = context.Database
			.GetPendingMigrations()
			.ToArray();

		if (!migrations.Any())
		{
			logger.LogInformation("Database is up to date.");
			return builder;
		}

		logger.LogInformation($"Apply migrations: {string.Join(", ", migrations)}");

		context.Database.Migrate();

		return builder;
	}
}
