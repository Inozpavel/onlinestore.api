﻿namespace OnlineStore.Api.Options;

public class JwtOptions
{
	public string Issuer { get; set; }

	public string SecretKey { get; set; }

	public int ExpireIntervalInMinutes { get; set; }
}
