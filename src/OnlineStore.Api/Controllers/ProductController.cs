﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OnlineStore.Api.Application.Enums;
using OnlineStore.Api.Application.Requests.Cart;
using OnlineStore.Api.Application.Requests.Product.Add;
using OnlineStore.Api.Application.Requests.Product.Get;
using OnlineStore.Api.Client.Models;
using OnlineStore.Api.Client.Models.Products;

namespace OnlineStore.Api.Controllers;

[ApiController]
[Route("api/products")]
public class ProductController : ControllerBase
{
	private readonly IMediator _mediator;

	public ProductController(IMediator mediator)
	{
		_mediator = mediator;
	}

	/// <summary>
	/// Получение списка товаров
	/// </summary>
	[HttpGet]
	[ProducesResponseType(typeof(CollectionModel<ProductResponseModel>), StatusCodes.Status200OK)]
	public async Task<IActionResult> GetProductsList([FromQuery] GetProductListRequest request)
	{
		var result = await _mediator.Send(request);
		return Ok(result);
	}

	/// <summary>
	/// Получение товара по id
	/// </summary>
	[HttpGet("{id:guid}")]
	[ProducesResponseType(typeof(ProductResponseModel), StatusCodes.Status200OK)]
	public async Task<IActionResult> GetProductsList([FromRoute] Guid id)
	{
		var result = await _mediator.Send(new GetProductRequest(id));
		return Ok(result);
	}

	/// <summary>
	/// Добавление товара в корзину 
	/// </summary>
	[Authorize]
	[HttpPost("{id:guid}/add-to-cart")]
	[ProducesResponseType(typeof(ProductResponseModel), StatusCodes.Status200OK)]
	public async Task<IActionResult> GetProductsList([FromRoute] Guid id, [FromQuery] int count)
	{
		var request = new AddProductToCartRequest
		{
			ProductId = id,
			Count = count
		};
		await _mediator.Send(request);
		return Ok();
	}

	/// <summary>
	/// Добавление товара в каталог
	/// </summary>
	[HttpPost]
	[Authorize(Policy = nameof(RolePolicy.MinimumModeratorRole))]
	[ProducesResponseType(typeof(ProductResponseModel), StatusCodes.Status200OK)]
	public async Task<IActionResult> AddProduct([FromBody] AddProductRequest request)
	{
		await _mediator.Send(request);
		return Ok();
	}
}
