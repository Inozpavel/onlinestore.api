﻿using System.Security.Claims;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OnlineStore.Api.Application.Enums;
using OnlineStore.Api.Application.Requests.Users.Profile;
using OnlineStore.Api.Application.Requests.Users.Register;
using OnlineStore.Api.Application.Requests.Users.Token;
using OnlineStore.Api.Client.Models.Users;
using Swashbuckle.AspNetCore.Annotations;

namespace OnlineStore.Api.Controllers;

[ApiController]
[Route("api/users")]
[SwaggerTag("Операции с пользователями")]
public class UserController : ControllerBase
{
	private readonly IMediator _mediator;

	public UserController(IMediator mediator)
	{
		_mediator = mediator;
	}

	/// <summary>
	/// Регистрация пользователя в системе
	/// </summary>
	[HttpPost("register")]
	[ProducesResponseType(StatusCodes.Status200OK)]
	[ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
	public async Task<IActionResult> Register([FromBody] RegisterUserRequest request)
		=> Ok(await _mediator.Send(request));

	/// <summary>
	/// Получение токена по почте и паролю
	/// </summary>
	[HttpPost("token")]
	[ProducesResponseType(typeof(TokenResponseModel), StatusCodes.Status200OK)]
	[ProducesResponseType(typeof(string), StatusCodes.Status404NotFound)]
	public async Task<IActionResult> GetToken([FromBody] GetTokenRequest request)
		=> Ok(await _mediator.Send(request));


	/// <summary>
	/// Получение информации о текущем пользователе по токену
	/// </summary>
	[Authorize]
	[HttpGet("current/profile")]
	[ProducesResponseType(typeof(ProfileResponseModel), StatusCodes.Status200OK)]
	[ProducesResponseType(StatusCodes.Status401Unauthorized)]
	public async Task<IActionResult> GetProfile()
	{
		var userId = User.FindFirst(x => x.Type == ClaimTypes.NameIdentifier)!.Value;
		return Ok(await _mediator.Send(new GetProfileRequest(Guid.Parse(userId))));
	}

	/// <summary>
	/// Получение списка ролей в системе
	/// </summary>
	[Authorize]
	[HttpGet("roles")]
	public IActionResult GetRoles() => Ok(Enum.GetNames(typeof(UserRole)).ToArray());
}
