﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OnlineStore.Api.Application.Requests.Orders;
using OnlineStore.Api.Client.Models;
using OnlineStore.Api.Client.Models.Orders;

namespace OnlineStore.Api.Controllers;

[ApiController]
[Route("api/orders")]
public class OrderController : ControllerBase
{
	private readonly IMediator _mediator;

	public OrderController(IMediator mediator)
	{
		_mediator = mediator;
	}

	/// <summary>
	/// Получение списка заказов
	/// </summary>
	[HttpGet]
	[Authorize]
	[ProducesResponseType(typeof(CollectionModel<OrderResponseModel>), StatusCodes.Status200OK)]
	public async Task<IActionResult> GetOrders([FromQuery] GetOrdersListRequest request)
	{
		var result = await _mediator.Send(request);
		return Ok(result);
	}

	/// <summary>
	/// Закрытие активного заказа
	/// </summary>
	[HttpPost("close-active")]
	[Authorize]
	[ProducesResponseType(StatusCodes.Status200OK)]
	public async Task<IActionResult> CloseActiveOrder()
	{
		var result = await _mediator.Send(new CloseActiveOrderRequest());
		return Ok(result);
	}
}
