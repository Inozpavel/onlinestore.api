﻿using OnlineStore.Api.Client.Services;

namespace OnlineStore.Api.Client;

public interface IOnlineStoreClient :
	IProductService,
	IOrderService,
	IUserService
{
}
