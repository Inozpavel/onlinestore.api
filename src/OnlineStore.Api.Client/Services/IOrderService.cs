﻿using OnlineStore.Api.Client.Models;
using OnlineStore.Api.Client.Models.Orders;
using RestEase;

namespace OnlineStore.Api.Client.Services;

public interface IOrderService
{
	[Get("api/promo-codes")]
	Task<CollectionModel<OrderResponseModel>> GetOrders(GetOrdersListRequestModel requestModel);
}
