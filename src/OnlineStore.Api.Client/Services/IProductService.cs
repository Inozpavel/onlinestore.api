﻿using OnlineStore.Api.Client.Models;
using OnlineStore.Api.Client.Models.Products;
using RestEase;

namespace OnlineStore.Api.Client.Services;

[SerializationMethods(Query = QuerySerializationMethod.Serialized)]
public interface IProductService
{
	[Get("api/products")]
	Task<CollectionModel<ProductResponseModel>> GetProducts([Query(null)] GetProductListRequestModel model);

	[Post("api/products")]
	Task AddProduct([Body] AddProductRequestModel requestModel);
}
