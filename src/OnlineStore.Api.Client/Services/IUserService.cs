﻿using OnlineStore.Api.Client.Models.Users;
using RestEase;

namespace OnlineStore.Api.Client.Services;

public interface IUserService
{
	[Header("Authorization")]
	public string Token { get; set; }

	[Get("api/users/current/profile")]
	Task<ProfileResponseModel> GetProfile();

	[Post("api/users/register")]
	Task RegisterNewUser([Body] RegisterUserRequestModel requestModel);

	[Get("api/users/roles")]
	Task<string[]> GetRoles();

	[Post("api/users/token")]
	Task<TokenResponseModel> GetToken([Body] GetTokenRequestModel requestModel);
}
