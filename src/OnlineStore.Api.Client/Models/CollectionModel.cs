﻿// ReSharper disable MemberCanBePrivate.Global

using Newtonsoft.Json;

namespace OnlineStore.Api.Client.Models;

public class CollectionModel<T> where T : class
{
	[JsonConstructor]
	public CollectionModel(ICollection<T> items)
	{
		Items = items;
	}

	public CollectionModel(ICollection<T> items, int length)
	{
		Items = items;
		Length = length;
	}

	public int ReturnedItemsLength => Items.Count;

	public int Length { get; }

	public ICollection<T> Items { get; }
}
