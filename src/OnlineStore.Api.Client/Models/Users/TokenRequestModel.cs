﻿namespace OnlineStore.Api.Client.Models.Users;

public class TokenRequestModel
{
	public string Email { get; set; }

	public string Password { get; set; }
}
