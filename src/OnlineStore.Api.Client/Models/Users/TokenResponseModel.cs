﻿namespace OnlineStore.Api.Client.Models.Users;

public class TokenResponseModel
{
	public string TokenType { get; set; }

	public string Token { get; set; }

	public int Expiration { get; set; }
}
