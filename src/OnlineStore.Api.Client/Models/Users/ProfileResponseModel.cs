﻿namespace OnlineStore.Api.Client.Models.Users;

public class ProfileResponseModel
{
	public string Username { get; set; }

	public string Email { get; set; }

	public string Phone { get; set; }

	public DateTimeOffset Created { get; set; }

	public string RoleName { get; set; }
}
