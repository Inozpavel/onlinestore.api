﻿namespace OnlineStore.Api.Client.Models.Users;

public class GetTokenRequestModel
{
	public string Email { get; set; }

	public string Password { get; set; }
}
