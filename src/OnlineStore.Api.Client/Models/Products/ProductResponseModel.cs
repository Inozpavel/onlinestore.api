﻿namespace OnlineStore.Api.Client.Models.Products;

public class ProductResponseModel
{
	public Guid Id { get; private set; }

	public string Name { get; private set; }

	public string Description { get; private set; }

	public decimal Cost { get; private set; }

	public string Image { get; private set; } = null;
}
