﻿namespace OnlineStore.Api.Client.Models.Products;

public class AddProductToCartRequestModel
{
	public int Count { get; set; }
}
