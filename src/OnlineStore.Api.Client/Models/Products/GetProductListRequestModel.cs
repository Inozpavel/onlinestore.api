﻿namespace OnlineStore.Api.Client.Models.Products;

public class GetProductListRequestModel : PaginationModel
{
	public string Query { get; set; }	
}
