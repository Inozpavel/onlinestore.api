﻿namespace OnlineStore.Api.Client.Models.Products;

public class AddProductRequestModel
{
	public string Name { get; set; }

	public string Description { get; set; }

	public decimal Cost { get; set; }
}
