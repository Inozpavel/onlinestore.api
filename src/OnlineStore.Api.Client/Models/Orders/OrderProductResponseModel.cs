﻿using System;

namespace OnlineStore.Api.Client.Models.Orders;

public class OrderProductResponseModel
{
	public Guid ProductId { get; private set; }

	public string ProductName { get; private set; }

	public int Count { get; private set; }

	public decimal Cost { get; private set; }
}
