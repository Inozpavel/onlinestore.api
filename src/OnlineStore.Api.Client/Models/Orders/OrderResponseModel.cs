﻿namespace OnlineStore.Api.Client.Models.Orders;

public class OrderResponseModel
{
	public DateTimeOffset OrderTime { get; private set; }

	public decimal TotalCost => Products.Items.Sum(x => x.Count * x.Cost);

	public CollectionModel<OrderProductResponseModel> Products { get; set; }
}
