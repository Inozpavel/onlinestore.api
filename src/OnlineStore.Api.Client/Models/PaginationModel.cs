﻿namespace OnlineStore.Api.Client.Models;

public abstract class PaginationModel
{
	private readonly int _skip;
	private readonly int _take = 20;

	/// <summary>
	/// Количества записей для пропуска
	/// </summary>
	public int Skip
	{
		get => _skip;
		init
		{
			if (value < 0)
			{
				throw new ArgumentException("Value can't be less 0", nameof(Skip));
			}

			_skip = value;
		}
	}

	/// <summary>
	/// Количество записей, которое нужно взять
	/// </summary>
	public int Take
	{
		get => _take;
		init
		{
			if (value < 0)
			{
				throw new ArgumentException("Value can't be less 0", nameof(Take));
			}

			_take = value;
		}
	}
}
