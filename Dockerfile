﻿FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /app
COPY . .

RUN dotnet restore --configfile /app/Nuget.Config
RUN dotnet build -c Release -o /app/build
 
FROM build AS publish
RUN dotnet publish -c Release -o /app/publish
COPY .gitlab-ci.yml /app/publish

FROM base AS final

WORKDIR /app
COPY --from=publish /app/publish ./

ENV ASPNETCORE_HTTP_PORT=80
ENV ASPNETCORE_DEBUG_PORT=9054
EXPOSE 80
EXPOSE 9054

ENTRYPOINT ["dotnet", "OnlineStore.Api.dll"]
